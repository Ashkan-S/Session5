package parsa.session5;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class TextInputActivity extends AppCompatActivity {

    EditText wordToTranslate;
    TextView TranslatedWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_input);

        wordToTranslate = (EditText) findViewById(R.id.WordToTranslate);
        TranslatedWord = (TextView) findViewById(R.id.TranslatedWord);

        findViewById(R.id.Translate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TranslateEnToFr_HTTPURL();

                TranslateEnToFr_Async();
            }
        });

    }

    void TranslateEnToFr_HTTPURL() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL obj = new URL("https://glosbe.com/gapi/translate?from=eng&dest=fra&format=json&phrase=" + wordToTranslate.getText().toString() + "&pretty=true");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        parseResponse(response.toString());
                    }
                } catch (Exception e) {
                    Toast.makeText(TextInputActivity.this, "Error In Connecting to Internet", Toast.LENGTH_SHORT).show();
                }
            }
        }).start();
    }

    void TranslateEnToFr_Async() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://glosbe.com/gapi/translate?from=eng&dest=fra&format=json&phrase=" + wordToTranslate.getText().toString() + "&pretty=true", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(TextInputActivity.this, "Error in Async", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseResponse(responseString);
            }
        });
    }

    void parseResponse(String Response) {
        /*try {
            JSONObject query_obj = new JSONObject(Response);

            String tuc_str = query_obj.getString("tuc");
            JSONObject tuc_obj = new JSONObject(tuc_str);

            String first_str = tuc_obj.getString("");
            JSONObject first_obj = new JSONObject(first_str);

            String phrase_str = first_obj.getString("phrase");
            JSONObject phrase_obj = new JSONObject(phrase_str);

            final String translatedText = phrase_obj.getString("text");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TranslatedWord.setText(translatedText);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        String name = "";
        try {
            JSONObject query_obj = new JSONObject(Response);
            JSONArray cast = query_obj.getJSONArray("tuc");
            for (int i = 0; i < 1; i++) {
                JSONObject actor = cast.getJSONObject(i);
                name = actor.getString("phrase");
            }

            JSONObject phrase_obj = new JSONObject(name);
            final String translatedText = phrase_obj.getString("text");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TranslatedWord.setText(translatedText);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
